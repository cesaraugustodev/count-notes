/* adiciona mascara ao campo valor */
$("#value").maskMoney({ prefix: "R$ ", allowNegative: true, thousands: ".", decimal: ",", affixesStay: false });
/* função executada ao clicar no botão calcular */
$("#calc").on("click", function () {
  /* verifica se o campo do valor está vazio ou nulo */
  if ($("#value").val() == "" || $("#value").val() == null || $("#value").val() == undefined) {
    alert("Preencha o campo com o valor para poder calcular!");
  } else {
    window.valorInput = $("#value").val();
    /* removo todos os pontos inseridso pela pascara */
    var valor = $("#value").val().replace(/\./g, "");
    /* Pego apenas a parte inteira do valor*/
    var aux = valor.split(",")[0];
    /* armazeno a parte decimal*/
    window.decimal = "0." + valor.split(",")[1];
    /* transformo a string de entrada em um numero do tipo float que aceita casa decimal */
    valor = parseFloat(aux);
    window.data = {};
    calc(valor);
  }
});

$("#recalc").on("click", function () {
  window.location.href = "../count-notes";
});

function calc(value) {
  /* se o numero for maior ou igual ao valor de cada nota executa a função que verifica a quantidade
  de notas de cada valor e armazena na variavel global WINDOW.DATA */
  if (value >= 200) {
    window.data.notas200 = Math.trunc(value / 200);

    calc(calcResto(value, 200));
  } else if (value >= 100) {
    window.data.notas100 = Math.trunc(value / 100);
    calc(calcResto(value, 100));
  } else if (value >= 50) {
    window.data.notas50 = Math.trunc(value / 50);
    calc(calcResto(value, 50));
  } else if (value >= 20) {
    window.data.notas20 = Math.trunc(value / 20);
    calc(calcResto(value, 20));
  } else if (value >= 10) {
    window.data.notas10 = Math.trunc(value / 10);
    calc(calcResto(value, 10));
  } else if (value >= 5) {
    window.data.notas5 = Math.trunc(value / 5);
    calc(calcResto(value, 5));
  } else if (value >= 2) {
    window.data.notas2 = Math.trunc(value / 2);
    window.data.resto = calcResto(value, 2) + parseFloat(window.decimal);
    setInfoView();
  } else {
    window.data.resto = value + parseFloat(window.decimal);
    setInfoView();
  }
}

/* função que calcula o resto de cada divisão */
function calcResto(val, param) {
  /* efetua a divisão */
  var aux = val / param;
  /* transforma em String para poder deixar apenas a parte decimal */
  var aux2 = aux.toString();
  /* adiciona 0. a parte decimal removida atravez do split() */
  var resto = "0." + aux2.split(".")[1];
  /* multiplica pelo divisor para se descobrir quanto realmente sobrou da divisão */
  return parseFloat(resto) * param;
}
/* seta as informações na tela */
function setInfoView() {
  $("#note200").html(window.data.notas200 == undefined ? 0 : window.data.notas200);
  $("#note100").html(window.data.notas100 == undefined ? 0 : window.data.notas100);
  $("#note50").html(window.data.notas50 == undefined ? 0 : window.data.notas50);
  $("#note20").html(window.data.notas20 == undefined ? 0 : window.data.notas20);
  $("#note10").html(window.data.notas10 == undefined ? 0 : window.data.notas10);
  $("#note5").html(window.data.notas5 == undefined ? 0 : window.data.notas5);
  $("#note2").html(window.data.notas2 == undefined ? 0 : window.data.notas2);
  $("#resto").html(window.data.resto == undefined ? "Sobrou - R$" + 0 : "Sobrou - R$" + window.data.resto.toLocaleString("pt-BR"));
  $("#valorCalc").html("R$ " + window.valorInput);
  $(".page-action").hide();
  $(".result").show();
}
/* limpa as informações da tela */
/* function clearInfoView() {
  $("#note200").html("");
  $("#note100").html("");
  $("#note50").html("");
  $("#note20").html("");
  $("#note10").html("");
  $("#note5").html("");
  $("#note2").html("");
  $("#resto").html("");
  $("#valorCalc").html("");
} */
